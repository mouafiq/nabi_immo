from openerp.osv import fields,osv
from openerp import tools
class color:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

class arbapro_appartement(osv.osv):

    _inherit = "product.template"

    def get_std_price(self,cr,uid,ids,field,arg,context=None):
        res={}
        for o in self.browse(cr,uid,ids):
            res[o.id] = ((o.s_interieur or 0.0) * (o.prix_metre or 0.0)) + ((o.s_terrasse or 0.0) *  (o.prix_terrasse or 0.0)) + ((o.s_garage or 0.0) *  (o.prix_garage or 0.0))
        print color.HEADER +  str(res)
        return res or False
    
    def get_amount_tt(self,cr,uid,ids,field,arg,context=None):
        res={}
        for o in self.browse(cr,uid,ids):
            res[o.id] = ((o.s_interieur or 0.0) * (o.prix_metre or 0.0)) + ((o.s_terrasse or 0.0) *  (o.prix_terrasse or 0.0)) + ((o.s_garage or 0.0) *  (o.prix_garage or 0.0))
            if  o.list_price and o.list_price > 0.0:
                res[o.id] = o.list_price
        print color.HEADER +  str(res)
        return res or False
        
    def get_avance(self,cr,uid,ids,field,arg,context=None):
        res={}
       
        for o in self.browse(cr,uid,ids):
            p = o.amount_tt or o.std_price or 0.0
            #p = p - ((o.prix_garage and o.prix_garage) or 0.0)            
            
            
            
            t = (p * (o.type1.prc_avance or 0.0) / 100) or 0.0
            a = (o.mtt_avance and o.mtt_avance > 0.0) and o.mtt_avance or False
            t = a or t 
            #av    =  o.payment_a and sum([x.amount for x in o.payment_a if x.state not in ('draft','cancel') ]) or 0.0
            #avn   =  o.payment_n and sum([x.amount for x in o.payment_n if x.state not in ('draft','cancel') ]) or 0.0 
            #t = t - av - avn
            
            
            if t < 0.0 :
                t = 0.0
            res[o.id] = t or 0.0
            
        return res
        
    def get_avance_solde(self,cr,uid,ids,field,arg,context=None):
        res={}
       
        for o in self.browse(cr,uid,ids):
            p = o.amount_tt or o.std_price or 0.0
            #p = p - ((o.prix_garage and o.prix_garage) or 0.0)            
            
            
            
            t = (p * (o.type1.prc_avance or 0.0) / 100) or 0.0
            a = (o.mtt_avance and o.mtt_avance > 0.0) and o.mtt_avance or False
            t = a or t 
            av    =  o.payment_a and sum([x.amount for x in o.payment_a if x.state not in ('drafts','cancel') ]) or 0.0
            avn   =  o.payment_n and sum([x.amount for x in o.payment_n if x.state not in ('drafts','cancel') ]) or 0.0 
            t = t - av - avn
            
            
            if t < 0.0 :
                t = 0.0
            res[o.id] = t or 0.0
            
        return res        

    def get_surface(self,cr,uid,ids,field,arg,context=None):
        res={}
        for o in self.browse(cr,uid,ids):
            res[o.id] = o.s_interieur + o.s_terrasse 
        return res

    def get_amount_rest(self,cr,uid,ids,field,arg,context=None):
        res={}
       
        for o in self.browse(cr,uid,ids):
            t = o.amount_tt or o.std_price or 0.0
            a = o.payment_a and sum([x.amount for x in o.payment_a if x.state not in ('drafts','cancel')]) or 0.0
            b = o.payment_b and sum([x.amount for x in o.payment_b if x.state not in ('drafts','cancel')]) or 0.0
            n = o.payment_n and sum([x.amount for x in o.payment_n if x.state not in ('drafts','cancel')]) or 0.0
            res[o.id] = t - a - b - n 
           
        return res
        
    def payment_a_tt(self,cr,uid,ids,field,arg,context=None):
        res={}
       
        for o in self.browse(cr,uid,ids):
            
            a = o.payment_a and sum([x.amount for x in o.payment_a if x.state not in ('drafts','cancel')]) or 0.0
            n = o.payment_n and sum([x.amount for x in o.payment_n if x.state not in ('drafts','cancel')]) or 0.0
            
            res[o.id] = a + n
           
        return res    
    def payment_b_tt(self,cr,uid,ids,field,arg,context=None):
        res={}
       
        for o in self.browse(cr,uid,ids):
            
            b = o.payment_b and sum([x.amount for x in o.payment_b if x.state not in ('drafts','cancel')]) or 0.0
            res[o.id] = b
           
        return res

    def payment_tt(self,cr,uid,ids,field,arg,context=None):
        res={}
       
        for o in self.browse(cr,uid,ids):
            
            
            res[o.id] = o.payment_a_tt + o.payment_b_tt
           
        return res                      

    def get_amount_b(self,cr,uid,ids,field,arg,context=None):
        res={}
       
        for o in self.browse(cr,uid,ids):
            p   = o.amount_tt or o.std_price or 0.0
            p   = p - ((o.prix_garage and o.prix_garage) or 0.0)
            
            t   = p * (o.type1.prc_b or 0.0) / 100 or 0.0
            t   = o.mtt_b  or (t +  (o.prix_garage and o.prix_garage or 0.0) ) or 0.0            
            
            #t2  = o.payment_b and sum([x.amount for x in o.payment_b if x.state not in ('draft','cancel')]) or 0.0
            
            #res[o.id] =  ((t > t2) and ( t - t2 )) or 0.0
            res[o.id] = t or 0.0

        return res
    def get_amount_b_solde(self,cr,uid,ids,field,arg,context=None):
        res={}
       
        for o in self.browse(cr,uid,ids):
            t   = o.amount_b or 0.0
                 
            
            t2  = o.payment_b and sum([x.amount for x in o.payment_b if x.state not in ('drafts','cancel')]) or 0.0
            
            res[o.id] =  ((t > t2) and ( t - t2 )) or 0.0
            

        return res        
    
    def get_amount_a(self,cr,uid,ids,field,arg,context=None):
        res={}
       
        for o in self.browse(cr,uid,ids):
            p   = o.amount_tt or o.std_price or 0.0
            p2   = p - ((o.prix_garage and o.prix_garage) or 0.0) 
            t   = (o.mtt_b and (p - o.mtt_b)) or (p2 * (100 - (o.type1.prc_b or 0.0)) / 100 ) or 0.0
            #t2  = o.payment_a and sum([x.amount for x in o.payment_a if x.state not in ('draft','cancel')]) or 0.0
            #n   = o.payment_n and sum([x.amount for x in o.payment_n if x.state not in ('draft','cancel')]) or 0.0
            #res[o.id] = t  - t2 - n
            res[o.id] = t or 0.0

        return res
        
    def get_amount_a_solde(self,cr,uid,ids,field,arg,context=None):
        res={}
       
        for o in self.browse(cr,uid,ids):
            
            t   = o.amount_a or 0.0
            t2  = o.payment_a and sum([x.amount for x in o.payment_a if x.state not in ('drafts','cancel')]) or 0.0
            n   = o.payment_n and sum([x.amount for x in o.payment_n if x.state not in ('drafts','cancel')]) or 0.0
            res[o.id] = t  - t2 - n
            
        return res        

    _columns = {
        'zone'                :fields.many2one('product.category', 'Zone'),
        'immeuble'            :fields.many2one('product.category', 'Immeuble'),
        'entree'              :fields.many2one('product.category', 'Entree'),
        'etage'               :fields.many2one('product.category', 'Etage'),
        'orientation'         :fields.many2one('product.category', 'Orientation'),
        'vue'                 :fields.many2one('product.category', 'Vue'),
        'titre'               :fields.char('Titre'),
        'type1'               :fields.many2one('product.category', 'Type'),
        'usage'               :fields.many2one('product.category', 'Usage'),
        'salons'              :fields.integer('Nombre de pieces'),
        'chambres'            :fields.integer('Nombre de chambre'),
        'facades'             :fields.integer('Nombre de facades'),
        'balcons'             :fields.integer('Nombre de balcons'),
        'sdb'                 :fields.integer('Nombre de SDB'),
        's_interieur'         :fields.float('Surface interieure'),
        's_terrasse'          :fields.float('Surface terrasse totale'),
        's_total'             :fields.function( get_surface,
                                                string='Surface totale',
                                                type='float',
                                                store=True),
        's_titre'             :fields.float('Surface sur titre'),
        's_garage'            :fields.float('Surface garage'),
        'attachment_ids'      :fields.many2many('ir.attachment', 
                                                'appartement_ir_attachments_rel', 
                                                'appartment_id', 
                                                'attachment_id', 
                                                'Attachments'),
        'attachment_plan'     :fields.many2many('ir.attachment', 
                                                'appartement_ir_attachments_plan_rel', 
                                                'appartment_id', 
                                                'attachment_plan_id', 
                                                'Plan'),
        'std_price'           :fields.function( get_std_price,
                                                string="Prix standard", 
                                                type="float",
                                                store=True
                                                ),
        'prix_ventes'         :fields.float('Prix avec marge commerciale'),
        'prix_metre'          :fields.float('Prix du metre'),
        'prix_terrasse'       :fields.float('prix terrasse'),
        'prix_garage'         :fields.float('prix garage'),
        'no_metre'            :fields.char('No'),
        'no_terrasse'         :fields.char('No. Terrasse/Mezzanine'),
        'no_garage'           :fields.char('No. Garage'),
        'observation'         :fields.text('Observations'),
        'reservation'         :fields.many2one('res.partner',string='Reservation'),
        'prospect'            :fields.many2many('res.partner',
                                                'appartement_client_ref',
                                                'appartement_id',
                                                'client_id',
                                                'Prospect'),
        'payment_a'           :fields.one2many('account.voucher','article',
                                                string='Payment A', 
                                                domain=[('state','not in',['cancel' ]),('pay_type','=','a')]),
        'payment_b'           :fields.one2many('account.voucher','article',
                                                string='Payment B', 
                                                domain=[('state','not in',['cancel' ]),('pay_type','=','b')]),
        'payment_n'           :fields.one2many('account.voucher','article',
                                                string='Payment Notaire', 
                                                domain=[('state','not in',['cancel' ]),('pay_type','=','n')]),                                                
        'amount_rest'         :fields.function( get_amount_rest,
                                                string='Rest a payer',
                                                type='float'),
        'amount_b'            :fields.function( get_amount_b,
                                                string='Montant B',
                                                type='float'),
        'amount_a'            :fields.function( get_amount_a,
                                                string='Montant A',
                                                type='float'),
        'payment_b_tt'            :fields.function( payment_b_tt,
                                                string='Paiement B',
                                                type='float'),
        'payment_a_tt'            :fields.function( payment_a_tt,
                                                string='Paiement A',
                                                type='float'),
        'payment_tt'            :fields.function( payment_tt,
                                                string='Paiement Tt',
                                                type='float'),
        'amount_b_solde'      :fields.function( get_amount_b_solde,
                                                string='Solde B',
                                                type='float'),
        'amount_a_solde'      :fields.function( get_amount_a_solde,
                                                string='Solde A',
                                                type='float'),
        'desistement'         :fields.many2one('arbapro.desistement', 'article'),
        'prc_avance'          : fields.float('Mtt Avance', default=False ),
        'mtt_avance'          : fields.float('Mtt Avance', default=False ),
        'mtt_b'               : fields.float('Mtt -B-', default=False ),
        'avance'              :fields.function( get_avance,
                                                string='Montant avance',
                                                type='float'),
        'avance_solde'        :fields.function( get_avance_solde,
                                                string='Solde avance',
                                                type='float'),
                                                        
        'amount_tt'           :fields.function( get_amount_tt,
                                                string='Montant Tt.',
                                                type='float'),
        'state'               :fields.selection(   string       ="State",
                                        selection   =[('l','Libre'),('r','Reserve'),('v','Vendu')],
                                        default     ="l"),                                                
                            
    }

class arbapro_partner(osv.osv):
    
    _inherit = "res.partner"
    _columns = {
        'cin'                   :fields.char('CIN'),
        'profession'            :fields.char('Profession'),
        'attachment_ids'        :fields.many2many('ir.attachment', 'partner_ir_attachments_rel', 'partner_id', 'attachment_id', 'Attachments'),
        'attachment_contrat'    :fields.many2many('ir.attachment', 'partner_ir_attachments_contrat_rel', 'partner_id', 'attachment_contrat_id', 'Contrat'),
        'attachment_CIN'        :fields.many2many('ir.attachment', 'partner_ir_attachments_cin_rel', 'partner_id', 'attachment_cin_id', 'CIN'),
        'reservation'           :fields.one2many('product.template','reservation',string='Reservation'),
        'payment'               :fields.one2many('account.voucher','partner_id',string='Payment' ,domain=[('state','not in',['cancel' ]),('pay_type','=','a')]),
        'payment_b'             :fields.one2many('account.voucher','partner_id',string='Payment - B' ,domain=[('state','not in',['cancel' ]),('pay_type','=','b')]),
        'desistement'           :fields.many2one('arbapro.desistement', 'client'),
        }
        
class arbapro_payment(osv.osv):

    _inherit = "account.voucher"

    _columns= {
        'article':  fields.many2one('product.template', 'Article'),
        'desistement':  fields.many2one('arbapro.desistement', 'Desistement'),
        'pay_type': fields.selection(   string       ="Type de payment",
                                        selection   =[('a','A'),('b','B'),('n','Notaire')],
                                        default     ="a"),
                                        
    }

class arbapro_desistement(osv.osv):
    _name       = "arbapro.desistement"
    _columns    = {
        'article'       : fields.many2one('product.template', 'Article'),
        'client'        :fields.many2many('res.partner', 'Client'),
        'total_paye'    :fields.float('Total paye'),
        'total_paye_b'  :fields.float('Total paye - B'),
        'deduction'     :fields.float('Deduction'),
        'Payment'       :fields.one2many('account.voucher','desistement',
                                        string='Payment', 
                                        domain=[('state','not in',['cancel' ]),('pay_type','=','A')]),
    }

class product_categ_name(osv.osv):
    _inherit       = "product.category"
    _columns = {
        'prc_b' : fields.float('Prc. B'),
        'prc_avance' : fields.float('Prc. Avance'),
    }
    
    def name_get(self,cr,uid,ids,context=None):
        if context is None:
            context ={}
        res=[]
        record_name = self.browse(cr,uid,ids,context)
        for object in record_name:

                res.append((object.id,object.name))
                
        return res
    
    
arbapro_appartement()
