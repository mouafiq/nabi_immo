
from openerp import tools
from openerp.osv import fields, osv
from openerp.tools.translate import _

class wizard_modele(osv.osv_memory):

    _name = 'wizard.modele'
    _columns = {
        'modele':fields.many2one('product.modele','code modele'),
        'import':fields.integer('Numero import'),
    }
    def open_planning(self, cr, uid, ids, context=None):
        if context is None:
            context = {}
        data = self.read(cr, uid, ids, context=context)[0]
        ctx = context.copy()
        ctx['default_modele'] = data['modele'][0]
        print "#########################################################" + str(ctx)
        return {
            'domain': "[('modele.id', '=', '" + str(data['modele'][0]) + "')]",
            'name': 'Planning modele : %s' % str(data['modele'][1]),
            'view_type': 'form',
            'view_mode': 'tree,form',
            'res_model': 'coupe.planning',
            'type': 'ir.actions.act_window',
            'context': ctx,
        }