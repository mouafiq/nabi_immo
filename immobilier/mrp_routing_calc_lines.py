from openerp.osv import fields, osv
from datetime import datetime, timedelta
from openerp import netsvc


class mrp_routing_workcenter(osv.osv):
    _name = 'mrp.routing.workcenter'
    _inherit = 'mrp.routing.workcenter'
    
    def calc_ts(self,cr,uid,ids,field,arg,context=None):
        context= {}
        res = {}
        bf = 0
        for obj in self.browse(cr,uid,ids,context=context):
            io = 1
            if obj.eff == 0 :
                io = 0
            rt_obj = self.pool.get('mrp.routing.calc')
            rt_ids = rt_obj.search(cr, uid,[('routing_id','=',obj.routing_id.id)])
            rts = rt_obj.browse(cr, uid, rt_ids, context=None)
            for rt in rts:
                bf =  rt.bf
        
            if bf <> 0   :
                res[obj.id] =  round(io * obj.hour_nbr / bf,0)
            if bf == 0 or obj.eff == 0:
                res[obj.id] = ''
            
        return res
    def calc_prod(self,cr,uid,ids,field,arg,context=None):
        context= {}
        res = {}
        bf = 0
        for obj in self.browse(cr,uid,ids,context=context):
            io = 1
            if obj.eff == 0 :
                io = 0
        
            if obj.hour_nbr <> 0 :

                res[obj.id] =  round (io * 60 *100/ ( obj.hour_nbr) )
            if obj.eff == 0:
                res[obj.id] = ''
            
        return res    
    
    _columns = {
        'accessoire': fields.char('Accessoire'),
        'eff': fields.float('Effectif'),
        'observation': fields.text('Observation'),
        'routing_id_eq': fields.integer('Gamme'),
        'section': fields.char('Section'),
        'sequence_eq': fields.integer('Sequence equilibrage'),
        'ts': fields.function(calc_ts,'Taux de saturation', type='char'),
        'prod': fields.function(calc_prod,'Production horaire', type='char'),
    }
        


mrp_routing_workcenter()
